.TH FISCHE "1" "April 2010" "fische " "User Commands"
.SH NAME
fische \- standalone sound visualisation
.SH SYNOPSIS
.B fische
[options]
.SH OPTIONS
.IP

.PP
\fB\-D\fR \fB\-\-driver\fR \fIdriver\fR
.IP
Use the specified audio input driver. Presently \fIalsa\fR, \fIpulse\fR, \fIportaudio\fR and \fIdummy\fR are supported.
.PP
\fB\-d\fR \fB\-\-device\fR \fIdevice\fR
.IP
\fIdevice\fR names the pcm capture device to get the sound data from. On most systems, the default will do just fine. This Option is ignored with the PulseAudio driver. The PortAudio driver knows the special device \fIhelp\fR, which will spit out a list of all known devices.
.PP
\fB\-g\fR \fB\-\-geometry\fR \fIX\fRx\fIY\fR
.IP
\fIX\fR and \fIY\fR specify the width and height of the animation. Defaults to 800x400.
.PP
\fB\-v\fR \fB\-\-virtual\fR \fIX\fRx\fIY\fR
.IP
\fIX\fR and \fIY\fR specify the width and height of the application window. Use this to prevent your computer from trying to switch to non\-existent fullscreen resolutions. \fBCAUTION: when specifying a virtual geometry, set the actual geometry FIRST!\fR
.PP
\fB\-f\fR \fB\-\-fullscreen\fR
.IP
Start fische in fullscreen mode.
.PP
\fB\-e\fR \fB\-\-extra\-nervous\fR
.IP
Start fische in nervous mode.
.PP
\fB\-s\fR \fB\-\-fps\fR \fIfps\fR
.IP
\fIfps\fR specifies the target frames per second. The default, 30, is what fische is designed for
.PP
\fB\-1\fR \fB\-\-single\fR
.IP
Use only one CPU, even if there are more available
.PP
\fB\-\-exit\-on\-mouseevent\fR
.IP
Exit when a mouse button is clicked (useful mainly on touchscreens)
.PP
\fB\-n\fR \fB\-\-nowrite\fR
.IP
Do not update the configuration file with the last known working configuration.
.PP
\fB\-h\fR \fB\-\-help\fR
.IP
Display a basic help message.
.SH RUNTIME CONTROLS
.PP
At runtime, press \fBP\fR to pause, \fBF\fR to toggle fullscreen mode, \fBN\fR to toggle nervosity and \fBESC\fR to quit. With the --exit-on-mouseevent flag, fische does exactly that.
.SH KNOWN ISSUES
.PP
PortAudio is currently slow on Linux. If able, use a different driver.
.PP
On Windows, the actual frame rate is often quite different from the specified one. The actual rate is shown after exit. Use the -s flag to increase it to about 30.
.SH TROUBLESHOOTING (Linux only)
.PP
If you get an error like "\fBX Error of failed request: BadValue...\fR"
.IP
Try to start fische with the -v or --virtual flags and set X/Y to values that correspond to a fullscreen resolution that you know exists. For example: fische -g 1400x700 -v 1400x1050
.PP
If fische starts, but \fBwon't react to sound\fR
.IP
First of all, \fItry a different input driver\fR. If you are lucky, this already solves your problem.
.IP
If not, you now have to choose which driver you would like to get working:
.IP
\fBpulse:\fR Fische opens the default source as set with pavucontrol or similar tools. For example, if you would like to visualize "what's playing", set the corresponding "monitor of output XYZ" device as default.
.IP
\fBalsa:\fR You might be using the wrong ALSA device. By default, fische tries to open the "default" device. It should be correctly configured on most systems, but with some soundcards you need complex ALSA configuration to achieve recording capabilities - look into the ALSA documentation. For example, soundcards with ICE1712 chips record internally produced sound on channels 10 and 11 instead of 0 and 1.
.IP
\fBalsa:\fR Recording might not be enabled. Check with "alsamixer" or an other mixer application. If you are trying to visualize sounds produced by an audio player, you must enable recording of what's called "PCM" on most cards. For external input, record "Line In", and so on...
.IP
\fBalsa:\fR Your sound card might not support recording of your chosen source. In this case, you are in bad luck. Most notably, many C-Media based cards do not allow recording while SPDIF out is in use. You can try a setup using an "aloop" dummy card - but that process is far beyond the scope of this man page, and with most distributions, it requires kernel or ALSA re-compilation. However, in this case the \fBPulseAudio input driver might still give you the desired results.\fR
.SH FILES
$HOME/.fischerc - automatically updated on clean exit.
.SH SEE ALSO
Runtime control documentation, troubleshooting information and FAQs in the README
.SH AUTHOR
This manual page was written by Marcel Ebmer fische@26elf.at. Permission is granted to copy, distribute and/or modify this document under the terms of the GNU General Public License, Version 2, or any later version published by the Free Software Foundation 
