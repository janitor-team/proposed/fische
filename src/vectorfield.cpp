
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "vectorfield.h"

#define N_FIELDS 20

using namespace std;


VectorField* VectorField::ms_instance = 0;
const double Pi = acos (-1);

VectorField::VectorField (int x, int y, double* progress)
{
	m_Width = x;
	m_Height = y;
	m_Diagonal =  (int) (.5 + sqrt (pow ( (double) m_Width, 2) + pow ( (double) m_Height, 2)));
	m_CenterX = m_Width / 2;
	m_CenterY = m_Height / 2;

	pExportData = NULL;

	if (progress) *progress = 0;

	int n = 0;
	uint16_t* new_field = fillField (n);
	if (new_field) cout << "\n* calculating vectorfields: 5%";
	if (progress) *progress = 1.0f / N_FIELDS;

	while (new_field) {
		m_Fields.push_back (new_field);
		n++;
		new_field = fillField (n);
		if (new_field)
			cout << "\r* calculating vectorfields: " << ( (n + 1) * 100) / N_FIELDS << "%";
		fflush (stdout);

		if (progress) *progress = ( (double) (n + 1)) / N_FIELDS;
	}

	if (m_Fields.empty()) {
		cerr << "ERROR: unable to calculate any vectorfields. out of memory?" << endl;
		exit (EXIT_FAILURE);
	}

	pExportData = m_Fields.at (18);
	cout << "\n* successfully calculated " << m_Fields.size() << " different vectorfields\n" << endl;
}

VectorField::~VectorField()
{
	for (unsigned int i = 0; i < m_Fields.size(); i++) {
		free (m_Fields[i]);
	}
	m_Fields.clear();
}

VectorField* VectorField::instance (int x, int y, double* progress)
{
	if (!ms_instance) ms_instance = new VectorField (x, y, progress);
	return ms_instance;
}

void VectorField::release()
{
	delete ms_instance;
	ms_instance = 0;
}

char* VectorField::get()
{
	return (char*) pExportData;
}

void VectorField::change()
{
	int n = rand() % m_Fields.size();
	while (pExportData == m_Fields[n]) n = rand() % m_Fields.size();
	pExportData = m_Fields[n];
}

int VectorField::width() const
{
	return m_Width;
}

int VectorField::height() const
{
	return m_Height;
}

void VectorField::randomize (Vector& vec, int n)
{
	return;
	if (n % 2 == 0) n++;
	Vector randv ( (rand() % n) - n / 2, (rand() % n) - n / 2);
	vec += randv;
}

uint16_t* VectorField::fillField (int f)
{
	uint16_t* field = (uint16_t*) malloc (m_Width * m_Height * 2);
	if (!field) return NULL;
	for (int x = 0; x < m_Width; x++) {
		for (int y = 0; y < m_Height; y++) {
			uint16_t* vector = field + x + y * m_Width;

			Vector p (x - m_CenterX, y - m_CenterY);
			Vector e = p.singleUnit();
			Vector n = e.normal();
			double r = p.length();

			Vector p1;
			if (m_Width >= m_Height) p1 = Vector (x - m_CenterX / 3, y - m_CenterY);
			else p1 = Vector (x - m_CenterX, y - m_CenterY / 3);
			Vector e1 = p1.singleUnit();
			Vector n1 = e1.normal();
			double r1 = p1.length();

			Vector p2;
			if (m_Width >= m_Height) p2 = Vector (x - m_Width * 5 / 6, y - m_CenterY);
			else p2 = Vector (x - m_CenterX, y - m_Height * 5 / 6);
			Vector e2 = p2.singleUnit();
			Vector n2 = e2.normal();
			double r2 = p2.length();

			Vector v;
			switch (f) {
				case 0:
					// linear vectors showing away from a horizontal mirror axis
					v = Vector (0, (y < m_CenterY) ? (yMax() / 100) : (-yMax() / 100));
					break;
				case 1:
					// linear vectors showing away from a vertical mirror axis
					v = Vector ( (x < m_CenterX) ? (xMax() / 100) : (-xMax() / 100), 0);
					break;
				case 2:
					// radial vectors showing away from the center
					v = e * -r / 25;
					break;
				case 3:
					// tangential vectors (right)
					v = n * r / 25;
					break;
				case 4:
					// tangential vectors (left)
					v = n * -r / 25;
					break;
				case 5:
					// tangential-radial vectors (left)
					v = e * -r / 25 - n * r / 25;
					break;
				case 6:
					// tangential-radial vectors (right)
					v = e * -r / 25 + n * r / 25;
					break;
				case 7: {
					// tree rings
					double dv = cos (Pi * 20 * r / dimension());
					v = e * 5 * dv;
					break;
				}
				case 8: {
					// hyperbolic vectors
					Vector _e (e.y(), e.x());
					v = _e * -r / 25;
					break;
				}
				case 9: {
					// hyperbolic vectors
					Vector _e (e.y(), e.x());
					v = _e * r / 25;
					break;
				}
				case 10:
					// purely random
					v = Vector (rand() % 21 - 10, rand() % 21 - 10);
					break;
				case 11: {
					// sphere
					double dv = cos (Pi * r / dimension());
					v = e * -r * dv / 15;
					break;
				}
				case 12: {
					// sine distortion
					double dv = sin (Pi * 6 * r / dimension());
					v = e * -r * dv / 25;
					break;
				}
				case 13:
					// black hole
					v = n * dimension() / 50 + e * r / 50;
					if (r < 10) v = Vector (0, 0);
					break;
				case 14:
					// black hole
					v = n * -dimension() / 50 + e * r / 50;
					if (r < 10) v = Vector (0, 0);
					break;
				case 15: {
					// circular waves
					double dim = 121 * pow (Pi, 2) / dimension();
					double vx = r * dim;
					v = e * -10 * sqrt (1.04 - pow (cos (sqrt (vx)), 2) + 0.25 * pow (sin (sqrt (vx)), 2));
					break;
				}
				case 16:
					// spinning CD
					if (fabs (r - (dimension() / 4)) < dimension() * 1 / 6) v = n * r / 25;
					//if ( (int) (sqrt (sqrt (r * 200 / dimension() ) ) ) % 2 == 0) v = n * r / 25;
					else v = n * -r / 25;
					break;
				case 17:
					// three spinning disks
				{
					double rt = (m_Width > m_Height) ? m_Width / 6 : m_Height / 6;
					if (r < rt * 1.2) {
						v = n * -r / 15;
					} else if (r1 < rt) {
						v = n1 * r1 / 15;
					} else if (r2 < rt) {
						v = n2 * r2 / 15;
					} else v = Vector (rand() % 21 - 10, rand() % 21 - 10);
					break;
				}
				case 18:
					// experimental
					v = e1 * (diagonal() - r1) / 100 + e2 * (diagonal() - r2) / 100 + e * (diagonal() - r) / -100;
					break;
				case 19:
					// experimental
					v = n1 * (diagonal() - r1) / -100 + n2 * (diagonal() - r2) / -100 + n * (diagonal() - r) / 100;
					break;
				default:
					// index too high. return nothing.
					free (field);
					return NULL;
			}
			randomize (v, 2);
			validate (v, x, y);
			*vector = v.toUint16();
		}
	}
	return field;
}

int VectorField::dimension() const
{
	return (m_Width > m_Height) ? m_Height : m_Width;
}

int VectorField::diagonal() const
{
	return m_Diagonal;
}
