
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include "ringbuffer.h"
using namespace std;

RingBuffer* RingBuffer::ms_instance = 0;

RingBuffer::RingBuffer (int size) {
	if (size % 2 != 0) size ++;
	mp_Buffer = (char*) malloc (size);
	mp_ExportBuffer = (char*) malloc (size);
	if ( (!mp_ExportBuffer) || (!mp_Buffer) ) {
		cerr << "ERROR: cannot allocate memory for ringbuffer" << endl;
		exit (EXIT_FAILURE);
	}
	m_Cursor = 0;
	m_IsLocked = false;
	m_Size = size;
	m_IsFull = false;
	m_Inserted = 0;
	cout << "* created ringbuffer of size " << m_Size << endl;
}

RingBuffer::~RingBuffer() {
	free (mp_Buffer);
	free (mp_ExportBuffer);
}

void RingBuffer::release() {
	delete ms_instance;
	ms_instance = 0;
}

RingBuffer* RingBuffer::instance (int size) {
	if (!ms_instance) ms_instance = new RingBuffer (size);
	return ms_instance;
}

void RingBuffer::insert (char* src, int n) {
	if (n > m_Size) {
		cout << "ERROR: cannot insert " << dec << n << " bytes into ringbuffer of size " << dec << m_Size << endl;
		exit (EXIT_FAILURE);
	}
	while (m_IsLocked) usleep (89);
	m_IsLocked = true;
	int d = m_Size - m_Cursor;
	if (d >= n) memcpy (mp_Buffer + m_Cursor, src, n);
	else {
		memcpy (mp_Buffer + m_Cursor, src, d);
		memcpy (mp_Buffer, src + d, n - d);
	}
	m_Cursor += n;
	while (m_Cursor >= m_Size) m_Cursor -= m_Size;
	m_Inserted += n;
	if (m_Inserted >= m_Size) m_IsFull = true;
	m_IsLocked = false;
}

char* RingBuffer::get() {
	m_Inserted = 0;
	m_IsFull = false;
	while (m_IsLocked) usleep (97);
	m_IsLocked = true;
	int d = m_Size - m_Cursor;
	memcpy (mp_ExportBuffer, mp_Buffer + m_Cursor, d);
	memcpy (mp_ExportBuffer + d, mp_Buffer, m_Size - d);
	m_IsLocked = false;
	return mp_ExportBuffer;
}

int RingBuffer::size() const {
	return m_Size;
}

bool RingBuffer::ready() const {
	return m_IsFull;
}
