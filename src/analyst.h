/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ANALYST_H
#define ANALYST_H

#include <vector>

class RingBuffer;
class Configuration;

class Analyst
{
	private:
		static Analyst* ms_instance;
		
	private:	
		Analyst();
		~Analyst();

	public:
		static Analyst* instance();
		void release();
		
	public:	
		int analyse();
		double framesPerBeat() const {
			return m_FramesPerBeat;
		};
		double relativeEnergy() const {
			return m_MovingAverage03 / m_MovingAverage30;
		}


	private:
		enum beatState 			{WAITING, MAYBEWAITING, BEAT};
		
		RingBuffer*				mp_RingBuffer;
		Configuration*			mp_Configuration;
		
		enum beatState			m_State;
		double					m_MovingAverage30;
		double					m_MovingAverage03;
		double					m_StandardDeviation;
		double					m_IntensityMa;
		double					m_IntensityStd;
		double					m_FramesPerBeat;
		int32_t					m_FrameNumber;
		int32_t					m_LastBeatFrame;
		std::vector<int32_t> 	m_BeatGapHistory;
		
		double level (int16_t*);
		double guessFramesPerBeat();
};

#endif // ANALYST_H
