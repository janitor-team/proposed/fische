
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <stdint.h>
#include "driver.h"
#include "configuration.h"

using namespace std;

Configuration* Configuration::ms_instance = 0;

Configuration::Configuration (int count, char** args)
{
	mp_AudioDevice = (const char*) malloc (64);
	strcpy ( (char*) mp_AudioDevice, "default");
	m_Width = 0;
	m_Height = 0;
	m_VirtualWidth = 0;
	m_VirtualHeight = 0;
	m_IsFullscreen = false;
	m_IsNervous = false;
	m_Fps = 30;
	m_IsMultiThreaded = true;
	m_DoExitOnMouse = false;
	m_DoWriteConfig = true;
#if defined(HAVE_PULSE)
	m_AudioDriver = PULSE;
#elif defined(HAVE_ALSA)
	m_AudioDriver = ALSA;
#elif defined(HAVE_PORTAUDIO)
	m_AudioDriver = PORTAUDIO;
#else
	m_AudioDriver = DUMMY;
#endif

	read_configfile();

	static struct option long_options[] = {
		{
			"geometry", required_argument, 0, 'g'
		}, {"virtual", required_argument, 0, 'v'}, {"device", required_argument, 0, 'd'}, {"driver", required_argument, 0, 'D'}, {"fullscreen", no_argument, 0, 'f'}, {"fps", required_argument, 0, 's'}, {"single", no_argument, 0, '1'}, {"extra-nervous", no_argument, 0, 'e'}, {"exit-on-mouseevent", no_argument, 0, 'x'}, {"nowrite", no_argument, 0, 'n'}, {"help", no_argument, 0, 'h'}, {0, 0, 0, 0}
	};

	int option_index = 0;
	int c;
	while ( (c = getopt_long (count, args, "g:v:d:D:fs:1pneh", long_options, &option_index)) != -1) {
		switch (c) {
			case 'g':
				set_geometry (optarg);
				break;
			case 'v':
				set_virtual (optarg);
				break;
			case 'd':
				set_device (optarg);
				break;
			case 'D':
				set_driver (optarg);
				break;
			case 'f':
				set_fullscreen ( (char*) ("true"));
				break;
			case 's':
				set_fps (optarg);
				break;
			case 'e':
				set_nervous ( (char*) ("true"));
				break;
			case '1':
				set_single ( (char*) ("true"));
				break;
			case 'x':
				set_eom ( (char*) ("true"));
				break;
			case 'n':
				m_DoWriteConfig = false;
				break;
			case 'h':
			case '?':
				help();
			default:
				break;
		}
	}
	if (!m_Width) m_Width = 800;
	if (!m_Height) m_Height = 400;
	if (!m_VirtualWidth) m_VirtualWidth = m_Width;
	if (!m_VirtualHeight) m_VirtualHeight = m_Height;

	if (m_IsMultiThreaded) {
		m_ProcessorCores = cpu_detect();
		cout << "* CPU cores: " << m_ProcessorCores << endl;
		if (m_ProcessorCores > 1) cout << "* using multi-threaded blur algorithm" << endl;
		else cout << "* using single-threaded blur algorithm" << endl;
	} else {
		m_ProcessorCores = 1;
		cout << "* CPU detection disabled on your request" << endl;
	}
}

Configuration::~Configuration()
{
	if (m_DoWriteConfig) write_configfile();
}

Configuration* Configuration::instance (int count, char** args)
{
	if (!ms_instance) ms_instance = new Configuration (count, args);
	return ms_instance;
}

void Configuration::release()
{
	delete ms_instance;
	ms_instance = 0;
}

void Configuration::toggleFullscreen()
{
	if (m_IsFullscreen) m_IsFullscreen = false;
	else m_IsFullscreen = true;
}

void Configuration::help()
{
	cout << endl;
	cout << "Usage: fische [-D (driver)] [-d (device)] [-g (X)x(Y)] [-v (X)x(Y)] [-f]" << endl;
	cout << "       [-e] [-s (fps)] [-1] [-n] [-h]" << endl;
	cout << endl;
	cout << "Details:" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-D --driver (driver)        Use the specified audio input driver. Presently" << endl;
	cout << "                            \"alsa\", \"pulse\", \"portaudio\" and \"dummy\"" << endl;
	cout << "                            are supported." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-d --device (device)        (device) names the pcm capture device to get" << endl;
	cout << "                            the sound data from. On most systems, the default" << endl;
	cout << "                            will do just fine." << endl;
	cout << "                            This option is ignored with the\"pulse\" audio" << endl;
	cout << "                            and \"dummy\" audio drivers." << endl;
	cout << "                            \"portaudio\" knows the special device \"help\", which" << endl;
	cout << "                            will spit out all devices available." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-g --geometry (X)x(Y)       (X) and (Y) specify the width and height of the" << endl;
	cout << "                            animation. default: 800x400" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-v --virtual (X)x(Y)        (X) and (Y) specify the width and height of the" << endl;
	cout << "                            application window. Use this to prevent your" << endl;
	cout << "                            computer from trying to switch to non-existent" << endl;
	cout << "                            fullscreen resolutions." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-f --fullscreen             Start up in fullscreen mode. At runtime, you can" << endl;
	cout << "                            toggle fullscreen mode by pressing 'F'." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-e --extra-nervous          Use nervous mode. This brings more life to the" << endl;
	cout << "                            animation. However, not too many music genres go" << endl;
	cout << "                            well with it. Toggle by pressing 'N'." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-s --fps (fps)              (fps) specifies the target frames per second." << endl;
	cout << "                            The default, 30, is what fische is designed for" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-1 --single                 Use only one CPU, even if there are more available" << endl;
	cout << "                            CPU detection is currently Linux-only." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "   --exit-on-mouseevent     Exit when a mouse button is clicked" << endl;
	cout << "                            (useful mainly for touchscreens)" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-n --nowrite                Do not update configuration file HOME/.fischerc" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "-h --help                   This message." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "RUNTIME CONTROLS ARE DOCUMENTED IN THE README AND ON THE MAN PAGE." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	exit (EXIT_FAILURE);
}

void Configuration::set_geometry (char* value)
{
	if (!strstr (value, "x")) {
		cerr << "ERROR: " << value << " is not a valid geometry string" << endl;
		help();
	}
	sscanf (value, "%dx%d", &m_Width, &m_Height);
	if ( (m_Width < 16) || (m_Width > 4096) || (m_Height < 16) || (m_Height > 4096)) {
		cerr <<	"ERROR: goemetry string invalid" << endl;
		help();
	}
	cout << "* using custom animation geometry " << m_Width << " by " << m_Height << endl;
}

void Configuration::set_virtual (char* value)
{

	if (!strncmp (value, "geometry", 8)) return;
	if (!strstr (value, "x")) {
		cerr << "ERROR: " << value << " is no valid virtual geometry string" << endl;
		help();
	}
	sscanf (value, "%dx%d", &m_VirtualWidth, &m_VirtualHeight);
	if ( (m_VirtualWidth < 16) || (m_VirtualWidth > 4096) || (m_VirtualHeight < 16) || (m_VirtualHeight > 4096)) {
		cerr <<	"ERROR: virtual goemetry string invalid" << endl;
		help();
	}
	if ( (m_VirtualWidth < m_Width) || (m_VirtualHeight < m_Height)) {
		cerr << "ERROR: virtual cannot be smaller than actual geometry" << endl;
		cerr << "       Specify a smaller geometry FIRST" << endl;
		help();
	}
	cout << "* using custom virtual geometry " << m_VirtualWidth << " by " << m_VirtualHeight << endl;
}

void Configuration::set_device (char* value)
{
	strncpy ( (char*) mp_AudioDevice, value, 64);
	cout << "* using custom capture device: \"" << mp_AudioDevice << "\"" << endl;
}

void Configuration::set_driver (char* value)
{
	if (!strcmp (value, "alsa")) m_AudioDriver = ALSA;
	else if (!strcmp (value, "pulse")) m_AudioDriver = PULSE;
	else if (!strcmp (value, "portaudio")) m_AudioDriver = PORTAUDIO;
	else if (!strcmp (value, "dummy")) m_AudioDriver = DUMMY;
	else {
		cout << "* unknown audio driver \"" << value << "\"." << endl;
		exit (EXIT_FAILURE);
	}
	switch (m_AudioDriver) {
		case ALSA:
			cout << "* using ALSA input" << endl;
			break;
		case PORTAUDIO:
			cout << "* using PortAudio input" << endl;
			break;
		case PULSE:
			cout << "* using PulseAudio input" << endl;
			break;
		case DUMMY:
		default:
			cout << "* WARNING: using dummy audio input" << endl;
			break;
	}
}

void Configuration::set_fps (char* value)
{
	sscanf (value, "%d", &m_Fps);
	cout << "* using custom target fps = " << m_Fps << endl;
}

void Configuration::set_fullscreen (char* value)
{
	if (!strcmp (value, "true")) {
		m_IsFullscreen = true;
		cout << "* fullscreen mode" << endl;
	} else m_IsFullscreen = false;
}

void Configuration::set_nervous (char* value)
{
	if (!strcmp (value, "true")) {
		m_IsNervous = true;
		cout << "* nervous mode" << endl;
	} else m_IsNervous = false;
}

void Configuration::set_single (char* value)
{
	if (!strcmp (value, "true")) {
		m_IsMultiThreaded = false;
		cout << "* using single-threaded algorithms on user request" << endl;
	} else m_IsMultiThreaded = true;
}

void Configuration::set_eom (char* value)
{
	if (!strcmp (value, "true")) m_DoExitOnMouse = true;
	else m_DoExitOnMouse = false;
}

void Configuration::read_configfile()
{
	m_ConfigFileIncludesExitOnMouse = false;
	m_ConfigFileIncludesSingleCPU = false;

#ifdef __WIN32__
	const char *homedir = getenv ("USERPROFILE");
#else
	const char *homedir = getenv ("HOME");
#endif // __WIN32__

	if (!homedir) return;

	cout << "\n* ----START OF CONFIG FILE----" << endl;
	string filename = string (homedir) + string ("/.fischerc");
	fstream configfile (filename.c_str(), fstream::in);
	if (configfile.is_open()) {
		cout << "* using saved configuration from " << filename << endl;
		char line[256];
		char option[256];
		char value[256];
		while (!configfile.getline (line, 256).eof()) {
			if (strlen (line) < 3) continue;
			if (line[0] == '#') continue;
			sscanf (line, "%s = %s", option, value);
			if (!strcmp (option, "geometry")) set_geometry (value);
			else if (!strcmp (option, "virtual")) set_virtual (value);
			else if (!strcmp (option, "device")) set_device (value);
			else if (!strcmp (option, "driver")) set_driver (value);
			else if (!strcmp (option, "fullscreen")) set_fullscreen (value);
			else if (!strcmp (option, "nervous")) set_nervous (value);
			else if (!strcmp (option, "fps")) set_fps (value);
			else if (!strcmp (option, "single")) {
				m_ConfigFileIncludesSingleCPU = true;
				set_single (value);
			} else if (!strcmp (option, "exit-on-mouseevent")) {
				m_ConfigFileIncludesExitOnMouse = true;
				set_eom (value);
			}
		}
		configfile.close();
		cout << "* -----END OF CONFIG FILE-----\n" << endl;
	}
}

void Configuration::write_configfile()
{
#ifdef __WIN32__
	const char *homedir = getenv ("USERPROFILE");
#else
	const char *homedir = getenv ("HOME");
#endif // __WIN32__

	if (!homedir) return;

	string filename = string (homedir) + string ("/.fischerc");
	fstream configfile (filename.c_str(), fstream::out);
	if (configfile.is_open()) {
		cout << "* saving configuration to " << filename << endl;

		configfile << "#############################" << endl;
		configfile << "# fische configuration file #" << endl;
		configfile << "#############################" << endl << endl;

		configfile << "geometry           = " << dec << m_Width << "x" << dec << m_Height << endl;

		if ( (m_VirtualWidth == m_Width) && (m_VirtualHeight == m_Height))
			configfile << "virtual            = geometry" << endl;
		else
			configfile << "virtual            = " << dec << m_VirtualWidth << "x" << dec << m_VirtualHeight << endl;

		configfile << "device             = " << mp_AudioDevice << endl;

		configfile << "fps                = " << dec << m_Fps << endl;

		if (m_AudioDriver == ALSA)
			configfile << "driver             = alsa" << endl;
		else if (m_AudioDriver == PULSE)
			configfile << "driver             = pulse" << endl;
		else if (m_AudioDriver == PORTAUDIO)
			configfile << "driver             = portaudio" << endl;

		if (m_IsFullscreen)
			configfile << "fullscreen         = true" << endl;
		else
			configfile << "fullscreen         = false" << endl;

		if (m_IsNervous)
			configfile << "nervous            = true" << endl;
		else
			configfile << "nervous            = false" << endl;

		configfile << "### uncomment to set single-CPU mode ###" << endl;
		if (m_ConfigFileIncludesSingleCPU) {
			if (!m_IsMultiThreaded)
				configfile << "single             = true" << endl;
			else
				configfile << "single             = false" << endl;
		} else configfile << "#single             = true" << endl;

		configfile << "### uncomment to enable exit on mouse click ###" << endl;
		if (m_ConfigFileIncludesExitOnMouse) {
			if (m_DoExitOnMouse)
				configfile << "exit-on-mouseevent = true" << endl;
			else
				configfile << "exit-on-mouseevent = false" << endl;
		} else configfile << "#exit-on-mouseevent = true" << endl;
	}
}

#if defined(__x86_64__) /* OK to use cpuid */
void host_get_cpuid (uint32_t t, uint32_t* eax, uint32_t* ebx, uint32_t* ecx, uint32_t* edx)
{
	asm ("pushq %%rbx\n\t"
	     "movl %%esi, %%ebx\n\t"
	     "cpuid\n\t"
	     "movl %%ebx, %%esi\n\t"
	     "popq %%rbx"
	     : "=a" (*eax)
	     , "=S" (*ebx)
	     , "=c" (*ecx)
	     , "=d" (*edx)
	     : "a" (t)
	     , "c" (0)
	     , "S" (0)
	     , "d" (0));
}
#elif defined(__i386__) /* OK to use cpuid */
void host_get_cpuid (uint32_t t, uint32_t* eax, uint32_t* ebx, uint32_t* ecx, uint32_t* edx)
{
	asm ("pushl %%ebx\n\t"
	     "movl %%esi, %%ebx\n\t"
	     "cpuid\n\t"
	     "movl %%ebx, %%esi\n\t"
	     "popl %%ebx"
	     : "=a" (*eax)
	     , "=S" (*ebx)
	     , "=c" (*ecx)
	     , "=d" (*edx)
	     : "a" (t)
	     , "c" (0)
	     , "S" (0)
	     , "d" (0));
}
#endif

int Configuration::cpu_detect()
{

#if defined(__i386__) || defined(__x86_64__) /* OK to use cpuid */
	int n = 1;
	uint32_t eax, ebx, ecx, edx;
	host_get_cpuid (0x0, &eax, &ebx, &ecx, &edx);
	char cpuid[13];
	strncpy (cpuid, (char*) &ebx, 4);
	strncpy (cpuid + 4, (char*) &edx, 4);
	strncpy (cpuid + 8, (char*) &ecx, 4);
	cpuid[12] = '\0';

	cout << "* CPU ID is: " << cpuid << endl;

	if (!strcmp (cpuid, "AuthenticAMD")) {
		host_get_cpuid (0x80000000, &eax, &ebx, &ecx, &edx);
		if (eax < 0x80000008) return 1;
		host_get_cpuid (0x80000008, &eax, &ebx, &ecx, &edx);
		n = (ecx & 0xff) + 1;
	} else { /* if it's not AMD, it's most likely Intel */
		host_get_cpuid (0x00000000, &eax, &ebx, &ecx, &edx);
		if (eax < 4) return 1;
		host_get_cpuid (0x4, &eax, &ebx, &ecx, &edx);
		n = (eax >> 26) + 1;
	}
	return n;

#else /* don't have cpuid */
	cout << "* CPU detection not available, assuming 1 core" << endl;
	return 1;
#endif
}
