#ifndef VECTOR_H
#define VECTOR_H

#include <stdint.h>
#include <cmath>

class Vector
{
	private:
		double m_X;
		double m_Y;

	public:
		enum direction {LEFT, RIGHT};

		Vector (double x = 0, double y = 0) {
			m_X = x;
			m_Y = y;
		}

		// accessors
		double x() const {
			return m_X;
		}
		double y() const {
			return m_Y;
		}

		// vector properties
		inline double length() {
			return sqrt (pow (m_X, 2) + pow (m_Y, 2));
		}

		// derived vectors
		Vector normal() {
			return (Vector (m_Y, -m_X));
		}
		Vector singleUnit() {
			return Vector (m_X / length(), m_Y / length());
		}

		// conversion to 2x uint8
		inline uint16_t toUint16() {
			uint8_t ix = (m_X < 0) ? (int) (m_X - 0.5) : (int) (m_X + 0.5);
			uint8_t iy = (m_Y < 0) ? (int) (m_Y - 0.5) : (int) (m_Y + 0.5);
			return (iy << 8) + ix;
		}

		// vector algebra
		Vector& operator+= (Vector& other) {
			m_X += other.x();
			m_Y += other.y();
			return *this;
		}
		Vector& operator-= (Vector& other) {
			m_X -= other.x();
			m_Y -= other.y();
			return *this;
		}
		Vector& operator/= (double d) {
			m_X /= d;
			m_Y /= d;
			return *this;
		}
		Vector& operator*= (double d) {
			m_X *= d;
			m_Y *= d;
			return *this;
		}

		Vector operator+ (Vector other) {
			return Vector (m_X + other.x(), m_Y + other.y());
		}
		Vector operator- (Vector other) {
			return Vector (m_X - other.x(), m_Y - other.y());
		}
		Vector operator* (double d) {
			return Vector (m_X * d, m_Y * d);
		}
		Vector operator/ (double d) {
			return Vector (m_X / d, m_Y / d);
		}

		Vector intersectBorder (Vector& nv, int x, int y, direction d) {
			x--;
			y--;
			Vector n = nv;
			if (d == RIGHT) n = Vector (-nv.x(), -nv.y());
			double t1, t2, t3, t4;
			if (n.x() == 0) {
				t1 = 1e6;
				t2 = 1e6;
			} else {
				t1 = -m_X / n.x();
				t2 = (x - m_X) / n.x();
			}
			if (n.y() == 0) {
				t3 = 1e6;
				t4 = 1e6;
			} else {
				t3 = -m_Y / n.y();
				t4 = (y - m_Y) / n.y();
			}
			t1 = (t1 < 0) ? 1e6 : t1;
			t2 = (t2 < 0) ? 1e6 : t2;
			t3 = (t3 < 0) ? 1e6 : t3;
			t4 = (t4 < 0) ? 1e6 : t4;
			double a = (t1 < t2) ? t1 : t2;
			double b = (t3 < t4) ? t3 : t4;
			double min_t = (a < b) ? a : b;
			if (min_t == 1e6) throw ("Fatal error in vector calculation");
			int rx = (int) (m_X + n.x() * min_t);
			while (rx < 0) rx ++;
			while (rx > x) rx --;
			int ry = (int) (m_Y + n.y() * min_t);
			while (ry < 0) ry ++;
			while (ry > y) ry --;
			return Vector (rx, ry);
		}
};

typedef Vector Point;

#endif // VECTOR_H
