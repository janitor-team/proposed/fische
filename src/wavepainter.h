
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WAVEPAINTER_H
#define WAVEPAINTER_H

#include <cmath>
#include "vector.h"

class SdlScreen;
class VectorField;
class RingBuffer;

class WavePainter
{
	private:
		static WavePainter* ms_instance;
		WavePainter ();

	public:
		void paint ();
		void beat (double bpm = 0);
		void changeColor (double bpm = 0, double energy = 0);
		void changeShape();

		static WavePainter* instance();

	private:
		inline void line (Vector& v1, Vector& v2, uint32_t c) {
			line ( (int) (m_X0 + v1.x()), (int) (m_Y0 + v1.y()), (int) (m_X0 + v2.x()), (int) (m_Y0 + v2.y()), c);
		}
		void line (int, int, int, int, uint32_t);

		int m_CenterX, m_CenterY;
		int m_Height;
		int m_Width;
		int m_Direction;
		int m_X0, m_Y0;
		SdlScreen* mp_SdlScreen;
		RingBuffer* mp_RingBuffer;
		int m_Shape;
		int m_Shapes;
		uint32_t m_Color1;
		uint32_t m_Color2;
		double m_Angle;
		bool m_IsRotating;
		double m_RotationIncrement;
};

int roundToInt (double);

#endif // WAVEPAINTER_H
