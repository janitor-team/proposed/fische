/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

class Configuration
{
	private:
		static Configuration* ms_instance;

	private:
		Configuration (int, char**);
		~Configuration();

	public:
		static Configuration* instance (int count = 0, char** args = 0);
		void release();

	public:
		enum audioDriver {ALSA, PULSE, DUMMY, PORTAUDIO};

		int fpsTarget() const {
			return m_Fps;
		}
		const char* audioDevice() const {
			return mp_AudioDevice;
		}
		int width() const {
			return m_Width;
		}
		int height() const {
			return m_Height;
		}
		int virtualWidth() const {
			return m_VirtualWidth;
		}
		int virtualHeight() const {
			return m_VirtualHeight;
		}
		bool fullscreen() const {
			return m_IsFullscreen;
		}
		bool multiThreaded() const {
			return m_IsMultiThreaded;
		}
		int processorCores() const {
			return m_ProcessorCores;
		}
		bool exitOnMouse() const {
			return m_DoExitOnMouse;
		}
		bool nervous() const {
			return m_IsNervous;
		}
		int audioDriver() const {
			return m_AudioDriver;
		}
		void toggleNervous() {
			m_IsNervous = m_IsNervous ? false : true;
		}

		void toggleFullscreen();

	private:
		const char*			mp_AudioDevice;
		int					m_Width;
		int					m_Height;
		int					m_VirtualWidth;
		int					m_VirtualHeight;
		bool				m_IsFullscreen;
		bool				m_IsNervous;
		int					m_Fps;
		bool				m_IsMultiThreaded;
		int					m_ProcessorCores;
		bool				m_DoExitOnMouse;
		enum audioDriver	m_AudioDriver;
		bool				m_DoWriteConfig;
		bool				m_ConfigFileIncludesExitOnMouse;
		bool				m_ConfigFileIncludesSingleCPU;

		void help();
		void set_geometry (char*);
		void set_nervous (char*);
		void set_virtual (char*);
		void set_device (char*);
		void set_driver (char*);
		void set_fullscreen (char*);
		void set_fps (char*);
		void set_single (char*);
		void set_eom (char*);
		void read_configfile();
		void write_configfile();
		int cpu_detect();

};

#endif // CONFIGURATION_H
