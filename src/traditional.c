/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include "traditional.h"

int traditional_blur (void* arg)
{
	blur_param_t* 	params 			= (blur_param_t*) arg;

	int8_t* 		vectors 		= params->vectors;
	int8_t*			vector_pointer	= NULL;

	const uint16_t 	width 			= params->width;
	const uint16_t 	width_x2 		= 2 * width;
	const uint16_t 	y_start 		= params->yMin;
	const uint16_t 	y_stop 			= params->yMax;

	const uint16_t 	surface_pitch 	= params->pitch;

	uint32_t* 		pixels 			= (uint32_t*) params->pixels;
	uint32_t* 		source_pixel;

	uint32_t*		destination_buffer			= (uint32_t*) params->buffer;
	uint32_t*		destination_pixel;

	uint32_t		source_component[4];

	const uint16_t	y0							= params->y0;
	const uint16_t	x0							= params->x0;

	const uint16_t	two_lines					= 2 * surface_pitch;
	const uint16_t	one_line					= surface_pitch;
	const uint16_t	two_columns					= 2;
	const uint16_t	one_column					= 1;

	uint16_t		x, y;
	int8_t			vector_x, vector_y;

	vector_pointer = vectors + y_start * width_x2;
	destination_pixel = destination_buffer + y_start * width;

	// vertical loop
	for (y = y_start; y < y_stop; y ++) {
		// horizontal loop
		for (x = 0; x < width; x ++) {
			
			// read the motion vector (actually its opposite)
			vector_x = * (vector_pointer + 0);
			vector_y = * (vector_pointer + 1);
			
			// point to the pixel at [present + motion vector]
			source_pixel = pixels + (y0 + y + vector_y) * surface_pitch + x0 + x + vector_x;
			
			// read the pixels at [source + (2,1)]   [source + (-2,1)]   [source + (0,-2)]
			// shift them right by 2 and remove the bits that overflow each byte
			source_component[0] = (* (source_pixel + one_line - two_columns) >> 2) & 0x3f3f3f3f;
			source_component[1] = (* (source_pixel + one_line + one_column) >> 2) & 0x3f3f3f3f;
			source_component[2] = (* (source_pixel - two_lines) >> 2) & 0x3f3f3f3f;
			source_component[3] = (* (source_pixel) >> 2) & 0x3f3f3f3f;
			
			// add those four components and write to the destination
			// increment destination pointer
			* (destination_pixel ++) = source_component[0]\
			                           + source_component[1]\
			                           + source_component[2]\
			                           + source_component[3];
			
			// increment vector source pointer
			vector_pointer += 2;
		}
	}
	return 0;
}
